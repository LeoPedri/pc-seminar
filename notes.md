# Pre-edge

Features

> Features in this region are due to electronic transitions to unoccupied
> virtual) states (basically an imaginary/virtual valence orbital is pictured.
> his can be largely localized on the absorbing atom itself but also delocalized
> n neighboring atoms).

Transitions

> **Dipole** allowed transitions (Δl = ±1) are the most probable transitions in
> this part of the spectrum. K-edges have the ground state electron in the _1s_
> (l = 0) orbital. Thus, for this transition, the density of the empty _p_
> states will be the most important (l = ±1). Similarly, for the L<sub>1</sub>
> edge involving transitions from the _2s_ orbital, the _p_ states will be
> the most relevant. For  the L<sub>2</sub> and L<sub>3</sub> edges (i.e.,
> orbitals of the _2p_ states), dipole allowed transitions include both _s_ and
> _d_ orbitals <mark>although those involving the _d_ states are the much  more
> likely. WHY?</mark>
> <br><br>
> Some transitions are (even partially) prohibited/inhibited by the geometry of
> the orbitals. Hence, hybridization and thus e.g., the ligands on a metal
> center, can play a huge role in determining what transitions  are allowed and
> what kind of signal intensity is visible in the pre-edge region. Sometimes
> these transitions can still happen, but must resort to a much less probable
> mechanism such as quadrupole transitions (Δl = ±2) to take place and will be
> much weaker in the spectrum.

# XANES

Near and above the edge-energy, excited electrons have attained Fermi energy
and are thus free to move within the material. Further, the mean free path of
the photoelectron at this energy level is very long (> 10 Å). This makes it
possible for the electron to be scattered by a large number of different atoms
(_multiple scattering_). This makes modeling and interpreting results in this
part of the spectrum very difficult. Most commonly, this problem is sort of
circumvented by employing a "finger print" approach where the spectrum is
compared to one of a known material.

Edge position

> Is usually estimated via the point of maximum first derivative of the
> absorption edge (i.e., highest slope). This method yields a point that is
> just above the Fermi energy of a metal and thus provides a very good
> starting point for the estimation of the threshold energy of the
> photoelectron.
> <br>
> The edge position strongly depends on the energy of the photo-excited
> electron and hence the respective core orbital. This means that it is very
> susceptible to the effective charge and its distribution of the core atom.
> For instance, if the oxidation  number of the absorbing atom is increased,
> then the energy levels of the core orbitals would be lowered, hence
> increasing their binding energies. The same holds true for an increase in
> electronegativity of the neighboring atoms.
> <br>
> Summing up, the position of the edge is derived by a mix of the perceived
> effective atomic number Z<sub>_eff_</sub>, the coordination geometry and the
> valence electron configuration (i.e., oxidation state) of a material. No
> absolute rule can be established at this point, as depending on the specific
> material and case studied, one of the aforementioned effects can dominate and
> completely subvert the trends hitherto dictated by the other effects.

# EXAFS

As can be seen from the spectra, in the EXAFS region oscillations can propagate
for hundreds of eV beyond the absorption edge. This is caused by
back-scattering of the photoelectron by electron clouds of neighboring
atoms. Depending on weather the back-scattered electron wave is in or out
of phase with the photoelectron, the probability of absorption increases or
decreases (in phase = increase). This results in a sinusoidal relationship
between the absorption and the wave vector _k_.

To generate EXAFS spectra, two base lines are need first. One involves the
determination of the background absorption pre-edge and the same after the
edge. For this application, the edge is completely ignored. Then, these two
baselines are corrected by the jump owed to the edge itself bringing the
pre-edge background and EXAFS (i.e., post edge) adsorption on one and the same
line. Finally, after taking the difference between the pre-edge background and the post-edge
observed absorption, the value is then divided once more by the pre-edge
background absorption to yield the measurement of interest χ(k) i.e., the
fractional change in absorption with respect to the wave vector _k_. EXAFS
results are determined with respect to _k_, since the electron-photoelectron
interaction can be/is treated as the interaction of two waves. The obtained
χ(k) vs. k spectrum is then multiplied by _k_<sup>_n_</sub> because otherwise
the oscillation would decay to quickly to see anything. Finally, a Fourier
transform reveals the magnitude, imaginary and most importantly the real part
of χ(k). The real part then should show a peak providing a measure for the
distance between the atom where the photoelectron was released from and the one
it interacted with.

Quantification

> A rather simplistic model for quantification is sufficient to provide an
> insight into the nature of the information that can be obtained from EXAFS
> data. This model assumes that only a single scattering process occurs i.e.,
> the photoelectron is considered to be scattered back from one atom traveling
> exactly twice the distance between the atom where it originates and where the
> back-scattering occurs. More realistic descriptions don't ignore higher order
> scattering (more than one scattering before returning to the central atom)
> and use a slightly different approach to describe the
> photoelectron.
> <br>
> Nonetheless, this model provides a less than perfect equation that still
> describes the dominant factors of EXAFS spectra. As mentioned before, the
> EXAFS data is sinusoidal. These oscillations turn out to be largely
> determined by the interatomic distance between the central absorbing atom and
> the scattering atom, thus granting access to the desired quantity. Another
> desired piece of information is the number of atoms in the surrounding shell
> where the photoelectron interacted (like a rdf). This information is
> extracted from the amplitude of the EXAFS features, since they are linearly
> related to the number of atoms in the shell. Finally, identifying the element
> present in said shell would be a precious insight. This can be done thanks to
> the characteristics of the back-scattering, which may be indicative for
> certain elements. The first of which is the back-scattering amplitude,
> containing information about the shape of the back-scattering. The role in
> identifying the element in the shell is given by the potential of the
> back-scattering atom, which heavily influences the amplitude.
> <br>
> Combined, all this information gathered form EXAFS spectra can determine the
> row of the periodic table wherein the back-scattering atom is to be found.
