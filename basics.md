X-rays
======

are photons with:
    * aproximate wavelength of 0.01–10 nm
    * frequencies in the range of 3×10^16 to 3×10^19 Hz
    * energies in the range of 100 eV to 100 keV

are produced by interaction of accelerated electrons with a metal target e.g.,
Cu, Fe, Mo, W, etc.
> two types of radiation are generated from a typical X-ray generating tube:
    * characteristic radiation
    * bremsstrahlung
> in the tube:
    * hot filament negative cathode (generates high-speed electrons)
    * positive metallic anode (where the X-rays are generated)
    (the anode is constantly rotated to dissipate heat)

X-ray creation (detailed)
* characteristic radiation
> 1. high energy electrons collide with metal's <u>inner shell</u> electrons.
> 2. the high energy electron leaves the atom with low energy (i.e., slow);
>    the inner shell electron also leaves the atom leaving a vacant site behind
> 3. the vacant site is filled by an outer shell electron with loss of energy
>    by emission of X-ray radiation.
> Depending on the shell of the vacant site and of the outer electron that fills
> it, a unique nomenclature is assigned to every transition to distinguish them.

* bremsstrahlung
> high energy electrons pass through the target material. If they don't collide
> with other electrons, they are deflected from their trajectory by the
> interaction positive nucleus. This results in a slight bend in their
> trajectory, as well as the emission of X-ray radiation.

A typical X-ray generation tube produces 80% of X-rays by bremsstrahlung
> this means that a wide spectrum of X-rays is generated.
> X-rays of the desired energy can be selected with filters.

The most characteristic feature of an X-ray is that it excites a core electron.
> offers local and element specific information

Two types of X-ray absorption spectroscopy (XAS) exist:
* X-ray absorption near-edge structure (XANES)
* extended X-ray absorption fine structure (EXAFS)
> XAS in general, deals with the change in an atom's X-ray absorption
> probability at energies near and above the binding energy of a core-level
> electron.


Starts to get more detailed here
================================

XAFS measures the absorption/transmission of X-rays as a function of input
radiation energy.
> A typical spectrum shows little to no absorption bevore a certain energy is
> reached. Then, a sudden peak (a.k.a. edge) emerges. This peak is
> characteristic for the type and chemical environment of the probed element
> and forms when the X-rays have sufficient energy to release electrons form
> the low-energy bound states of the probed atoms.

XAFS depends on/describes/is sensitive to the following properties of the
absorbing atom:
* physical state
* chemical state
* valence state
* bond distance
* coordination number
* species of the material which surrounds the probed elemnt

<u>Subdivision of XAFS spectra into regions</u>
i. **pre-edge** region: defined by E < E_0 where E_0 is the threshold energy of the
element edge
ii. **XANES** region: defined by E = E_0 ± 10 to 20 eV
iii. **EXAFS** region: defined by E_0 + 1.000 eV > E > E_0 + 30 eV

i. Pre-edge
> In most of the transition metal compounds, the metal K-edge spectra reflect the
> weak pre-edge peak due to the quadrupole-allowed but dipole-forbidden 1s → 3d
> excitation (i.e., Δl = ±2) <mark>Why is the dipole transition forbidden??
> What is the difference between a dipole and a quadrupole transition, only
> that in one case Δl = ± 1 and in the other ± 2? Why?</mark>
* usefulness of the pre-edge:
    - estimate <mark>ligand field effects</mark>
    - spin state
    - centrosymmetry
    <mark>WHY</mark>

ii. Rising-edge
> Metal K-rising-edge adsorptions originate from the dipole-allowed Δl = ± 1
> transitions.
* usefulness of the rising-edge:
    - estimate geometric structure
    - metal-ligand overlap via <mark>shakedown transitions</mark>
    - charge on the metal center
    <mark>WHY</mark>

ii. XAFS
> XAFS signals originate from interference between the outgoing and the
> reflected photoelectron waves from the central and surrounding atoms of a
> material.
* usefulness of the XAFS:
    - atomic scale structural information about the local environment
      surrounding the central absorbing atom (e.g., bond distance, coordination
      number, local symmetry)
    - local electronic/atomic structure of short-range materials e.g.,
      amorphous systems such as liquids and gases
    Local sensitivity very important for application, huge bonus for XAFS


(
    excursion: X-ray absorption and coefficient
    - absorption of X-rays via the photoelectri effect
    - only happens if the X-ray has E > E_binding of the electron to the core
    - absorption happens according to Lambert Beer law: I_t = I_0 * e^{-\mu t}
    t is sample thickness here!

    X-ray absorption coefficient is highly dependent on the atomic mass and the
    energy of incident radiation. => perfect for studying the difference e.g.,
    in coordination chemistry, between close atoms in the periodic table like
    Zn and Cu.

    The decay process of an electron into a vacant site in the core shells
    takes approx. a few femtoseconds. There are two remedies to this
    hole/vacant site:
    - Fluorescence: emission of X-rays at well-defined energies
    - <mark>Auger's effect</mark>: ejection of secondary electrons
)

XANES theory in too much detail at page 502
